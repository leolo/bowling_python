from src.frame import Frame

class Game:
    """
    A Game ends after 10 Frames.
    Frames will be added by passing in single rolls,
    or utilising the constructor.
    """
    __number_of_frames = 9  # starting with 0

    def __init__(self, frames=None):
        self._frames = []
        self.__add_frame()

        if frames:
            self.__handle_constructor(frames)

    def __handle_constructor(self, frames):
        for frame in frames:
            for roll in frame:
                self.add_roll(roll)

    def add_roll(self, pins):
        """
        Adds rolled pins to the frame.
        After a frame is completed the method expects 
        the next roll for a new frame.
        """
        self.is_over()

        if self._frames[self.__count_frames() - 1].frame_completed():
            self.__add_frame()

        self._frames[self.__count_frames() - 1].rolls = pins

    def __add_frame(self):
        frames_completed = self.__count_frames()

        if frames_completed == Game.__number_of_frames:
            frame = Frame(frames_completed, True)
        else:
            frame = Frame(frames_completed, False)

        self._frames.append(frame)

    def is_over(self):
        """Raises exception if game already finished"""
        if (self.__count_frames() - 1) == Game.__number_of_frames and \
            self._frames[self.__count_frames() - 1].frame_completed():
            raise Exception('Game is already over. Roll could not be added')

    def score(self):
        """Returns the game score as int"""
        score = 0

        for frame in self._frames:
            if frame.rolls:
                score += self.__sum_frame_score(frame)

        return score

    def __sum_frame_score(self, frame):
        strike = frame.strike()
        spare = frame.spare()
        bonus_points_available = self.__bonus_points_available(frame)

        if strike and bonus_points_available:  # add points of next two rolls
            return frame.get_points() \
            + self.__sum_bonus_points(self.__get_next_frame(frame), 2)
        elif spare and bonus_points_available: # add points of next roll
            return frame.get_points() \
            + self.__sum_bonus_points(self.__get_next_frame(frame), 1)
        return frame.get_points()

    def __bonus_points_available(self, frame):
        if not frame.is_last_frame and self.__is_next_frame_available(frame):
            return True
        else:
            return False

    def __sum_bonus_points(self, frame, no_of_bonus_rolls):
        strike, spare = 2, 1
        rolls = len(frame.rolls)

        if no_of_bonus_rolls == strike:
            if rolls == 1 and self.__is_next_frame_available(frame):
                return frame.get_points() \
                + self.__sum_bonus_points(self.__get_next_frame(frame), 1)
            elif rolls in [2, 3]:
                return frame.rolls[0] + frame.rolls[1]
            else:                               
                return frame.rolls[0]
        elif no_of_bonus_rolls == spare:
            return frame.rolls[0]

    def __is_next_frame_available(self, frame):
        next_frame = frame.frame_no + 1
        frames_rolled = self.__count_frames() - 1

        if next_frame <= frames_rolled:
            return True
        else:
            return False

    def __count_frames(self):
        return len(self._frames)

    def __get_next_frame(self, frame):
        return self._frames[frame.frame_no + 1]
