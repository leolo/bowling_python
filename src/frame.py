class Frame:
    """
    A Frame has between one and three rolls.
    The Frame ends after the first roll if all 10 pins get rolled (strike).
    If atleast all 10 pins in two rolls (spare)
    are rolled in the last Frame it's rewarded with a bonus roll.
    """
    def __init__(self, frame_no, last_frame):
        self.__rolls = []
        self.__frame_no = frame_no
        self.__last_frame = last_frame

    def __repr__(self):
        return str(self.__rolls)

    @property
    def rolls(self):
        return self.__rolls

    @rolls.setter
    def rolls(self, pins):
        if self.frame_completed():
            raise Exception('Frame is completed! Not able to add a roll')
        else:
            self.__add_roll(pins)

    @property
    def frame_no(self):
        return self.__frame_no

    @property
    def is_last_frame(self):
        return self.__last_frame

    def __add_roll(self, pins):
        if self.__is_valid_roll(pins):
            self.__rolls.append(pins)

    def __is_valid_roll(self, pins):
        if isinstance(pins, int) and pins in range(0, 11):
            return True
        else:
            raise Exception(
                'Input is invalid. Pins has to be in range of 0-10')

    def frame_completed(self):
        """Returns if frame is completed"""
        rolls_completed = len(self.__rolls)

        if rolls_completed == 1:
            return self.__frame_completed_after_first_roll()
        elif rolls_completed == 2:
            return self.__frame_completed_after_second_roll()
        elif rolls_completed == 3:
            return True
        return False

    def __frame_completed_after_first_roll(self):
        if self.strike() and not self.is_last_frame:
            return True
        else:
            return False

    def __frame_completed_after_second_roll(self):
        if not self.__is_bonus_roll_available():
            return True
        else:
            return False

    def __is_bonus_roll_available(self):
        atleast_spare = 10
        pins_rolled = self.get_points()

        if self.is_last_frame and pins_rolled >= atleast_spare:
            return True
        else:
            return False

    def strike(self):
        """Returns if frame has a strike"""
        strike = 10
        first_roll = self.__rolls[0]

        if first_roll == strike:
            return True
        return False

    def spare(self):
        """Returns if frame has a spare"""
        rolls_completed = len(self.__rolls)
        spare = 10

        if rolls_completed == 2 and self.get_points() == spare:
            return True
        return False

    def get_points(self):
        """
        Returns points of the frame as integer.
        Doesn't include bonus points.
        """
        frame_points = 0

        if not self.__rolls:
            return None

        for roll in self.__rolls:
            frame_points += roll
        return frame_points
