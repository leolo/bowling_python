import unittest
from src.frame import Frame

class Test_Frame(unittest.TestCase):
    def setUp(self):
        self.frame_1 = Frame(0, False)
        self.frame_1.rolls = 0

        self.frame_2 = Frame(0, False)
        self.frame_2.rolls = 0
        self.frame_2.rolls = 0

        self.frame_3 = Frame(0, False)
        self.frame_3.rolls = 10

        self.frame_4 = Frame(0, False)
        self.frame_4.rolls = 6
        self.frame_4.rolls = 4

        self.frame_5 = Frame(9, True)
        self.frame_5.rolls = 0
        self.frame_5.rolls = 9

        self.frame_6 = Frame(9, True)
        self.frame_6.rolls = 0
        self.frame_6.rolls = 10

        self.frame_7 = Frame(9, True)
        self.frame_7.rolls = 0
        self.frame_7.rolls = 10
        self.frame_7.rolls = 10

        self.frame_8 = Frame(9, True)
        self.frame_8.rolls = 10
        self.frame_8.rolls = 10
        self.frame_8.rolls = 10

        self.frame_9 = Frame(9, True)

    def test_rolls_setter_valid_input(self):
        try:
            self.frame_9.rolls = 10
        except:
            self.assertFalse('Input is invalid. Pins has to be in range 0-10')

    def test_rolls_setter_invalid_input(self):
        with self.assertRaises(Exception):
            self.frame_9.rolls = 11
    
    def test_frame_completed_first_roll(self):
        self.assertFalse(self.frame_1.frame_completed())

    def test_frame_completed_after_frame(self):
        self.assertTrue(self.frame_2.frame_completed())

    def test_frame_completed_after_strike(self):
        self.assertTrue(self.frame_3.frame_completed())

    def test_frame_completed_after_spare(self):
        self.assertTrue(self.frame_4.frame_completed())

    def test_frame_nine_points(self):
        self.assertTrue(self.frame_5.frame_completed())   

    def test_frame_bonus_roll(self):
        self.assertFalse(self.frame_6.frame_completed())
    
    def test_frame_after_bonus_roll(self):
        self.assertTrue(self.frame_7.frame_completed())

    def test_strike_zero_points(self):
        self.assertFalse(self.frame_2.strike())

    def test_strike(self):
        self.assertTrue(self.frame_3.strike())

    def test_strike_with_spare(self):
        self.assertFalse(self.frame_4.strike())

    def test_spare_zero_points(self):
        self.assertFalse(self.frame_2.spare())

    def test_spare_with_strike(self):
        self.assertFalse(self.frame_3.spare())

    def test_spare(self):
        self.assertTrue(self.frame_4.spare())
    
    def test_get_points_zero_points(self):
        self.assertEqual(self.frame_1.get_points(), 0)

    def test_get_points_strike(self):
        self.assertEqual(self.frame_3.get_points(), 10)

    def test_get_points_spare(self):
        self.assertEqual(self.frame_4.get_points(), 10)

    def test_get_points_one_roll_with_points(self):
        self.assertEqual(self.frame_5.get_points(), 9)

    def test_get_points_bonus_roll(self):
        self.assertEqual(self.frame_7.get_points(), 20)

    def test_get_points_maximum_points(self):
        self.assertEqual(self.frame_8.get_points(), 30)