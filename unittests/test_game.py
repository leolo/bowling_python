import unittest 
from src.game import Game

class Test_Game(unittest.TestCase):
    
    def setUp(self):
        self.game_1 = Game()
        self.game_2 = Game([[3]])
        self.game_3 = Game([[5, 4]])
        self.game_4 = Game([[10], [5, 4], [3, 2], [0, 0], [6, 4], [5, 2], 
                            [4, 4], [3, 5], [10], [10, 10]])
        self.game_5 = Game([[4, 6], [3, 4], [9, 0], [7, 1], [6, 4], [3, 4], 
                            [4, 5], [6, 2], [7, 1], [10, 10, 10]])
        self.game_6 = Game([[5, 5], [1, 6], [7, 3], [6, 2], [4, 3], [1, 2], 
                            [6, 4], [2, 6], [3, 4], [0, 0]])
        self.game_7 = Game([[3, 4], [7, 1], [5, 3], [3, 4], [4, 1], [7, 2], 
                            [3, 5], [4, 1], [4, 5], [0, 9]])
        self.game_8 = Game([[10], [10], [10], [10], [10], [10], [10], [10], 
                            [10], [10, 10, 10]])
        self.game_9 = Game([[10], [10], [10], [10], [10], [10]])

    def test_add_roll_null_pins(self):
        self.game_1.add_roll(0)

        self.assertEqual(repr(self.game_1._frames), '[[0]]')

    def test_add_roll_pins(self):
        self.game_2.add_roll(7)

        self.assertEqual(repr(self.game_2._frames), '[[3, 7]]')

    def test_add_roll_maximum_pins(self):
        self.game_3.add_roll(10)

        self.assertEqual(repr(self.game_3._frames), '[[5, 4], [10]]')

    def test_add_roll_invalid_number_of_pins(self):
        try:
            self.game_1.add_roll(11)
        except:
            self.assertTrue(Exception)

    def test_is_over_empty_game(self):
        try:
            self.game_1.is_over()
        except:
            self.assertFalse('Error: Game not finished')

    def test_is_over_first_roll(self):
        try:
            self.game_2.is_over()
        except:
            self.assertFalse('Error: Game not finished')

    def test_is_over_first_frame(self):
        try:
            self.game_3.is_over()
        except:
            self.assertFalse('Error: Game not finished')

    def test_is_over_bonus_roll_left(self):
        try:
            self.game_4.is_over()
        except:
            self.assertFalse('Error: Game not finished')

    def test_is_over_full_game_with_bonus_roll(self):
        self.assertRaises(Exception, self.game_5.is_over)

    def test_is_over_full_game_without_bonus_roll(self):
        self.assertRaises(Exception, self.game_6.is_over)

    def test_is_over_full_game_without_bonus_points(self):
        self.assertRaises(Exception, self.game_7.is_over)

    def test_is_over_full_game_maximum_points(self):
        self.assertRaises(Exception, self.game_8.is_over)

    def test_score_empty_game(self):
        self.assertEqual(self.game_1.score(), 0)

    def test_score_one_roll(self):
        self.assertEqual(self.game_2.score(), 3)

    def test_score_one_frame(self):
        self.assertEqual(self.game_3.score(), 9)

    def test_score_strike_and_spare(self):
        self.assertEqual(self.game_4.score(), 121)

    def test_score_full_game(self):
        self.assertEqual(self.game_5.score(), 112)

    def test_score_spare(self):
        self.assertEqual(self.game_6.score(), 79)

    def test_score_no_bonus_points(self):
        self.assertEqual(self.game_7.score(), 75)

    def test_score_maximum_points(self):
        self.assertEqual(self.game_8.score(), 300)  

    def test_score_uncompleted_strike_only(self):
        self.assertEqual(self.game_9.score(), 150)