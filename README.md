# My Python implementation of the clean code kata from Robert C. Martin.

The goal of this project is to write with test driven development clean code.

More informations about this problem can be found here: http://codingdojo.org/kata/Bowling/.

## Get the project
`git clone https://gitlab.com/leolo/bowling_python.git`

## Run tests
Change in to the cloned project directory.

`cd bowling_python`

Run tests.

`python3 -m unittest`